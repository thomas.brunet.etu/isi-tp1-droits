#include <crypt.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../question8/check-pass.h"

#define MAX_LEN 80
#define SALT "2B" //voir man 5 crypt

extern int check_password();
extern char* ask_password();
extern char* get_password();
extern char* getPasswordPath();

int register_password(char* password);
int update_password(char* password);

int main (){
    char *password;
    int res;

    if(strcmp(get_password(),"-1") == 0){ //get_password() renvoie -1 si elle ne trouve pas de mot de passe
        printf("Initialisation du mot de passe\n");
        password = ask_password();
        res = register_password(password);
    } else{
        if(check_password() != 0){ //mauvais mot de passe
            printf("Modification du mot de passe impossible\n");
            exit(EXIT_FAILURE);
        }
        printf("Saisie du nouveau mot de passe\n");
        password = ask_password();
        res = update_password(password);
    }

    if(res == 0){
        printf("Mot de passe enregistré avec succès\n");
        exit(EXIT_SUCCESS);
    } else{
        printf("Impossible de modifier le mot de passe\n");
        exit(EXIT_FAILURE);
    }

}

/*Enregistre un mot de passe pour un nouvel utilisateur*/
int register_password(char *password){
    FILE *f;
    char *seuid = malloc(16);

    sprintf(seuid,"%llu",geteuid()); //Récupération de l'euid de l'user et cast en string

    f = fopen(getPasswordPath(), "a"); /*Ouverture en mode append*/

    if(f == NULL){
        return -1;
    }

    fprintf(f,"%s,%s",seuid,crypt(password, SALT)); //ajout du couple euid,password au fichier

    free(seuid);
    fclose(f);
    return 0;
}

/*Mets à jour le mot de passe d'un utilisateur*/
int update_password(char *password){
    FILE *f;
    FILE *tmp;

    char *line = malloc(sizeof(char) * MAX_LEN);
    char *buffer = malloc(sizeof(char) * MAX_LEN);
    char *seuid = malloc(16);

    char *token;

    sprintf(seuid,"%llu",geteuid());

    f = fopen(getPasswordPath(), "r");
    tmp = fopen("tmp", "w");

    if(f == NULL || tmp == NULL){
        return -1;
    }

    while(fgets(buffer, MAX_LEN, f)){
        line = buffer;
        token = strtok(line,",");
        if(strcmp(token,seuid) != 0){
            fputs(buffer, tmp);
        }
    }

    fprintf(tmp,"%s,%s",seuid,crypt(password, SALT)); //ajout du couple euid,password au fichier

    fclose(f);
    fclose(tmp);

    remove(getPasswordPath());
    rename("tmp",getPasswordPath());
    
    free(line);
    free(seuid);

    return 0;
}
