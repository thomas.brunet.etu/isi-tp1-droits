# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Brunet Thomas thomas.brunet.etu@univ-lille.fr 

- Nison Jeremy jeremy.nison.etu@univ-lille.fr

## Question 1

Création de l'user toto : ```sudo adduser toto```

Ajout de toto au groupe ubuntu : ```sudo usermod -a -G ubuntu toto ```

toto a uniquement les drois de lecture sur le fichier. Le processus ne pourra donc pas ouvrir en écriture le fichier, car toto, qui a lancé le processus, ne possède pas les drois nécessaires.

## Question 2

Le caractère x indique qu'un répertoire est accessible pour un utilisateur ou groupe donné.

Une fois les drois d'execution retirés avec commande ```chmod```, toto ne peut plus accéder au répertoire, car ni toto ni son groupe (ubuntu) n'ont les droit d'execution sur ce répertoire, il ne peut donc plus y accéder.

La commande échoue car pour lister le contenu de mydir, l'initiateur de la commande ```ls mydir``` doit avoir les droit nécessaires (x) pour accéder au dossier mydir, ce qui n'est pas le cas de toto ou de son groupe.

## Question 3

(code source : ```question3/suid.c```)

Dans un premier temps, les différents ids valent tous 1001 (l'id de toto). Le processus ne parvient pas à ouvrir le fichier mydata.txt.

Une fois le flag ```set-user-id``` activé les valeurs de ruid,rgid et egit valent 1001 (l'id de toto) mais euid vaut 1000 (l'id de ubuntu). Le processus parvient alors à ouvrir le fichier.

## Question 4

(code source : ```question4/suid.py```)

A partir de Ubuntu on obtient uid : 1000 gid : 1000.  
A partir de toto on obtient uid : 1001 gid : 1001
## Question 5

La commande ```chnf``` sert à changer les informations de l'utilisateurs.

les droits de la commande sont : 
![droit_chfn](img/droit_chfn.png)

ils signifient qu chaque utilisateurs peut exécuter le programme mais le s qui est le sticky bit autrement dit le bit de restriction permet de ne pas pouvoir le supprimer 

informations de l'utilisateurs a :

![info_de_base](img/info_de_base_a.png)

informations de l'utilisateurs a après chnf : 

![info_modifie](img/info_modifie.png)
 

## Question 6

Les mots de passe des utilisateurs sont stockés dans ```ect/shadow``` car ils sont chiffrés et donc pas affichés directement dans ```etc/passwd```. 

## Question 7

Commandes réalisées : 
- Création des utilisateurs :
    - sudo adduser lambda_a
    - sudo adduser lambda_b
- Création des groupes : 
    - sudo groupadd groupe_a
    - sudo groupadd groupe_b
    - sudo groupadd groupe_c
- Ajout des utilisateurs dans leurs groupes respectifs :
    - sudo usermod -a -G groupe_a lambda_a
    - sudo usermod -a -G groupe_b lambda_b
    - sudo usermod -a -G groupe_c lambda_a
    - sudo usermod -a -G groupe_c lambda_b
- Changement des groupes propriétaires des répertoires :
    - sudo chgrp -R groupe_a dir_a
    - sudo chgrp -R groupe_b dir_b
    - sudo chgrp -R groupe_c dir_c
- Changemnet des drois des différents répertoires :
    - chmod 750 dir_c
    - chmod 770 dir_a
    - chmod 770 dir_b
- Changement des droits : seul les propriétaires des fichiers peuvent supprimer les fichiers qu'ils ont créé :
    - chmod +t dir_a
    - chmod +t dir_b

pour tester si les commandes effectuer donne le résultat attendu il suffit de :
- Lancer le script bash depuis l'utilisateur admin (ubuntu sur la vm) situé ```question7/createtxt.sh```
- Lancer le script bash depuis l'utilisateur lambda_a situé ```question7/lambda_a.sh```

Comme pour l'utilisateur b on effectue les mêmes commandes que l'utilisateur a, nous avont testés uniquement pour l'utilisateur a.

Pour l'utilisateur ubuntu (admin) il faut vérifier qu'il peut écrire dans le répertoire c et que seul lui peut modifier les fichiers comme ci dessous : 

![q7_proof](img/q7.png)

## Question 8

(code source : ```question8/```)

Usage : ```./rmg chemin/du/fichier```

Pour les tests effectués, on considère que l'utilsiateur admin est ubuntu. Les mots de passes sont stockés dans le fichier ```home/ubuntu/passwd``` sous la forme uid,mot_de_passe. Si vous souhaitez utiliser un chemin différent, il faudra le préciser dans la variable ```PASSWORDPATH``` de check-pass.c et recompiler avec ```make```.

Le fichier passwd a été configuré comme ceci :  
![password-config](img/config-passwd.png)

Voici un exemple d'execution ou l'utilisateur a supprime un fichier qu'il a créé dans le ```dir_a```, et l'utilisateur b supprime un fichier qu'il a créé dans le ```dir_b```, d'abord en rentrant un mauvais mot de passe. On constate bien la suppression (ou la non suppression) des fichiers après l'execution de la commande ```rmg```.  
![exec](img/exec.png)

Cette capture d'écran montre le fonctionnement du programme lorsqu'un utilisateur essaye de supprimer un fichier qu'il n'a pas créé. Ici, l'utilisateur a essaye de supprimer un fichier créé par ubuntu dans ```dir_a```. Le programme indique de l'accès au fichier à été refué, et celui-ci n'as pas été supprimé.  
![exec-fail](img/exec-fail.png)

## Question 9

(code source : ```question9/pwg.c```)

Modifications impélemntées dans check-pass.c : utilsiation de la fonction crypt.  
Le code source à été implémenté mais n'est pas fonctionnel pour le moment : on peut enregistrer uniquement un seul utilisateur

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








