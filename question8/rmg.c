#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "check-pass.h"

struct stat s;

extern int check_password();

int main (int argc, char* argv[]){
    int user_gid;

    if(argc != 2){
        printf("Usage : ./rmg [filepath]\n");
        exit(EXIT_FAILURE);
    }

    /*vérification droit d'acces*/
    if(stat(argv[1], &s) == -1){
        printf("STAT] impossible d'exectuer la commande stat\n");
        exit(EXIT_FAILURE);
    }

    user_gid = (int) getegid(); /*Récupération egid de l'user*/

    /*Comparaision du gid du fichier et de l'user*/
    if((int)s.st_gid != user_gid){
        printf("[GID CHECK] Accés au fichier refusé !\n");
        exit(EXIT_FAILURE);
    }

    /*vérification mot de passe*/
    if(check_password() != 0){
        printf("[PASSWORD] Accès au ficher refusé !\n");
        exit(EXIT_FAILURE);
    }

    /*Suppression du fichier*/
    if(remove(argv[1]) != 0){
        printf("Erreur lors de la suppression du fichier !\n");
        exit(EXIT_FAILURE);
    }

    printf("Fichier supprimé avec succés !\n");
    exit(EXIT_SUCCESS);
}
