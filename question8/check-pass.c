#include <crypt.h>
#include "check-pass.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#define PASSWORDPATH "passwd"
#define MAX_LEN 80
#define SALT "2B" //voir man 5 crypt

/*Renvoie 0 si le mot de passe entré par l'user correspond au mot de passe dans le fichier PASSWORDPATH/[euid], renvoie -1 sinon */
int check_password(){
    char* input;
    char* password;

    /*Demande de saisie du mot de passe*/
    input = ask_password();

    /*Récupération password dans le directory*/
    password = get_password();

    /*Comparaison du mot de passe saisi avec le mot de passe contenu dans le fichier PASSWORDPATH*/
    if(strcmp(password, crypt(input, SALT)) != 0){
        return -1;
    }

    return 0;
}

/*Renvoie le mot de passe saisi par l'utilisateur sur l'entrée standard*/
char* ask_password(){
    char *input = malloc(sizeof(char) * MAX_LEN); 

    printf("Mot de passe : ");
    fgets(input, MAX_LEN, stdin);
    
    return input;
}

/*Format de stockage des mots de passe : 1 fichier texte contenant des associations euid,password*/
/*Renvoie le mot de passe associé à l'euid de l'user, ou "-1" si aucun euid ne correspond dans le fichier*/
char* get_password(){
    char* buffer = malloc(sizeof(char) * MAX_LEN);
    char* password;
    char *token;
    char *seuid = malloc(16);
    FILE *f;

    sprintf(seuid,"%llu",geteuid()); //Récupération de l'euid de l'user et cast en string

    f = fopen(PASSWORDPATH,"r");

    while(fgets(buffer, MAX_LEN, f)){
        token = strtok(buffer,",");
        while(token != NULL){
            if(strcmp(token,seuid) == 0){
                token = strtok(NULL,",");
                password = token;
                fclose(f);
                return password;
            }
            token = strtok(NULL,",");
        }
    }
    
    fclose(f);
    return "-1"; //return "-1" si l'utilisateur n'existe pas dans le fichier
}

/*Renvoie PASSWORDPATH*/
char* getPasswordPath(){
    return PASSWORDPATH;
}
