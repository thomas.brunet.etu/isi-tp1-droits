    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/types.h>

    int printids(){
        uid_t ruid = getuid();
        uid_t euid = geteuid();
        gid_t rgid = getgid();
        gid_t egid = getegid();

        printf("uid : %d\n",(int)ruid);
        printf("gid : %d\n",(int)rgid);
        printf("euid : %d\n",(int)euid);
        printf("egid : %d\n",(int)egid);

        return 0;
    }

    int testopen(char* filepath){
        FILE *f;
        char c;

        f = fopen(filepath, "r");

        if(f == NULL){
            printf("Ouverture du fichier impoossible\n");
            exit(EXIT_FAILURE);
        }

        c = fgetc(f);
        while(c != EOF){
            printf("%c",c);
            c = fgetc(f);
        }

        fclose(f);
        return -1;
    }

    int main(int argc, char *argv[]){
        if(argc < 2){
            printf("Nombre d'arguments incorrect\n");
            printf("Usage : ./suid <chemin_du_fichier>\n");
            exit(EXIT_FAILURE);
        }
        printids();
        testopen(argv[1]);
        return 0;
    }
