all : makeall

makeall : 
	cd question3 && gcc -Wall -Wextra -o suid suid.c
	cd question8 && gcc -Wall -Wextra -o rmg check-pass.c rmg.c -lcrypt
	cd question9 && gcc -Wall -Wextra -o pwg ../question8/check-pass.c pwg.c -lcrypt
